# History
HISTSIZE=1000000
SAVEHIST=1000000
HISTFILE="$HOME/.cache/zsh/history"
setopt histignoredups

# Source aliases and cdable_vars if they are present on the system.
setopt cdable_vars
[ -f "$HOME/.config/zsh/.zsh_cdvars" ] && source "$HOME/.config/zsh/.zsh_cdvars"
[ -f "$HOME/.config/zsh/.zsh_aliases" ] && source "$HOME/.config/zsh/.zsh_aliases"

# Vi mode
bindkey -v
export KEYTIMEOUT=1
bindkey "^R" history-incremental-search-backward

setopt autocd           # Automatically cd into a typed path
stty stop undef         # Disable ctrl-s to freeze the terminal 

# Completion
autoload -Uz compinit 
compinit
zstyle ':completion:*' menu select

# Plugins
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
eval "$(starship init zsh)"
